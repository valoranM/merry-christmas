CC=gcc
CFLAGS=
LDFLAGS=-lncurses
EXEC=merry_christmas

all: $(EXEC)

merry_christmas: main.o
	$(CC) -o $@ $^ $(LDFLAGS)

main.o:

%.o: %.c
	$(CC) -o $@ -c $< $(CFLAGS) 2> /dev/null

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)